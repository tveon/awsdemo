#!/usr/bin/env bash

AWS_PROFILE=tveon
BUCKET_NAME=aws-js-demo

aws --profile ${AWS_PROFILE} s3 cp app s3://${BUCKET_NAME} --recursive