converter = new showdown.Converter();
src = "https://upload.wikimedia.org/wikipedia/commons/thumb/1/1d/AmazonWebservices_Logo.svg/500px-AmazonWebservices_Logo.svg.png";

$('#image').attr("src", src).one('load',function() {
    $(this).fadeIn(700);
}).each(function() {
    if (this.complete) $(this).trigger('load');
});

// See the Configuring section to configure credentials in the SDK
// AWS.config.credentials = ... ; Proper config, see: http://docs.aws.amazon.com/AWSJavaScriptSDK/guide/browser-configuring.html
AWS.config.update({accessKeyId: 'key', secretAccessKey: 'secret'});

// Configure your region
AWS.config.region = 'eu-west-1';

var bucketName= 'aws-js-demo';
var s3 = new AWS.S3();
s3.listObjects({Bucket: bucketName, Prefix: 'pages/'}, function(error, data) {
    if (error) {
        console.log(error); // an error occurred
    } else {
        //console.log(data); // request succeeded
        for (var i = 0; i < data.Contents.length; i++) {
            console.log(data.Contents[i].Key);
            var url = s3.getSignedUrl('getObject', {Bucket: bucketName, Key: data.Contents[i].Key});
            console.log('The URL is', url);

            $.get(url, function(data){
                var perLine=data.split('\n');
                for(i=0;i<perLine.length;i++) {
                    console.log(perLine[i]);
                }
                var html = converter.makeHtml(data);
                $("#content").html(html);
            });
        }
    }
});

