package dk.tveon.awsdemo.lambda;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.*;

/**
 * Lambda function for counting stuff - backed with DynamoDBs AtomicCounters
 * http://docs.aws.amazon.com/amazondynamodb/latest/developerguide/WorkingWithItems.html#WorkingWithItems.AtomicCounters
 */
public class DynamoCounter implements RequestHandler<String, Integer> {

    private static final String TABLE_NAME = "LambdaDemo";
    private static final String KEY = "Prefix";
    private static final String COUNTER = "Counter";

    public Integer handleRequest(String prefix, Context context) {
        //Make an instance for CloudWatch logs
        System.out.println("Counting " + prefix);

        AmazonDynamoDBClient dynamoDBClient = new AmazonDynamoDBClient();
        dynamoDBClient.setRegion(Region.getRegion(Regions.EU_WEST_1));

        System.out.println("Updating Dynamo DB");
        final UpdateItemResult updateResult = dynamoDBClient.updateItem(new UpdateItemRequest()
                .withTableName(TABLE_NAME)
                .withReturnValues(ReturnValue.UPDATED_NEW)
                .addKeyEntry(KEY, new AttributeValue(prefix))
                .addAttributeUpdatesEntry(COUNTER, new AttributeValueUpdate()
                        .withValue(new AttributeValue().withN("1"))
                        .withAction(AttributeAction.ADD)));
        System.out.println("Counted " + updateResult + " " + prefix);
        return Integer.valueOf(updateResult.getAttributes().get(COUNTER).getN());

    }

    public static void main(String... args) {
        DynamoCounter counter = new DynamoCounter();
        final Integer count = counter.handleRequest("web", null);
        System.out.println(count);
    }

}
